import numpy as np

tvec = np.linspace(0,2*np.pi, 500)

with open('circle.txt', 'w') as f:
    for t in tvec: 
        f.write('{} {} {}\n'.format(np.cos(t), np.sin(t), 0.0))

