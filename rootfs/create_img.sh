#!/bin/bash

if [ $# -ne 1 ]; then
	echo "Please provide the name of the flash image"
	exit 0
fi 

#create image first
dd if=/dev/zero of=$1 bs=1G count=1
DEVLOOP=$(sudo losetup --partscan --find --show $1)

#create the partition this way
(echo o; echo n; echo p; echo; echo; echo; echo; echo; echo w) | sudo fdisk $DEVLOOP;

sudo mkfs.ext4 ${DEVLOOP}p1
#sudo losetup -d $DEVLOOP
